<!DOCTYPE html>
<html>
<body>

    <h1>Buat Account Baru! </h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label><br>
        <input type="text" id="fname" name="fname"><br>
        <label for="lname">Last name:</label><br>
        <input type="text" id="lname" name="lname">

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="gender">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="malaysia">Malaysia</option>
            <option value="singapore">Singapore</option>
            <option value="amerika">Amerika</option>
          </select>


        <p>Language Spoken:</p>

     
          <input type="checkbox" id="html" name="fav_language" value="HTML">
          <label for="html">HTML</label><br>
          <input type="checkbox" id="css" name="fav_language" value="CSS">
          <label for="css">CSS</label><br>
          <input type="checkbox" id="javascript" name="fav_language" value="JavaScript">
          <label for="javascript">JavaScript</label>

          <p>Bio:</p>

          <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br> 
          <input type="submit" value="Submit">
      </form>

</body>
</html>